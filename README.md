

UNIVERSIDAD POLITECNICA DE TECAMAC


Nombre de la Materia: Aplicaciones Móviles


Nombre del Maestro: Emmanuel Torres Servín


Nombre del Alumno: David Antonio Olmos Ruiz


Grupo:2920IS


Tema: Practica (Examen)












Contenido
CREACION DEL SERVICIO	3
LOGIN	4
PETICION_LOGIN	7
CLASE API	8
SERVICIOPETICION.JAVA	10





















 
CREACION DEL SERVICIO

Como primer paso para crear nuestra aplicación tenemos que tomar en cuenta que todos los datos que insertemos en nuestra aplicación se irán directamente a una base de datos para almacenarlos y así poder tener un mejor control y hacer “n” numero de veces todas las peticiones que necesitemos a la aplicación.

Las URL que se tomaran en cuenta son:
https://notificacionupt.andocodeando.net/api/todasNot

Esta url la tendemos que poner en el programa “POSTMAN” aquí en este programa se harán todas las peticiones de manera GET y POST depende como la quiera y la requiera la aplicación, de igual manera se tendrán varios campos para este enlace, lo que son:
*Estado 
*Correo
*Password
*Token
Estos campos tendrán un tipo de dato especifico ya que se obtendrán diferentes valores dentro de la aplicación, quedarán de la siguiente manera:












LOGIN

Para crear el login de nuestra aplicación se tiene que crear el activity con el nombre “Login” de igual manera se crearán la clase y los XML con el mismo nombre, por recomendación propia y por buenas tareas, sugerimos siempre, guardar todos los activitys, peticiones o layouts en una carpeta ya que aquí será más fácil visualizarlos y no se tendrá ningún problema, también para ello tenemos que bajar 2 librerías esenciales:
*Retrofit
*Gson
Después procederemos a darle vista y diseño al activity del login, en esta parte pondremos los siguientes botones:
*Editext
*Editext
*TextView
*Button
*ImageView

Después acomodarlos a nuestro estilo, pasaremos a ponerles el nombre a cada botón, para que así los podamos identificar muy fácil en el momento de programarlo, se le asignara y quedaran de esta manera:
*Editext_Correo
*Edittext_Password
*BtnAvanzar

Ahora comenzaremos con la parte del código de la clase login:

Public class Login extends AppCompatActivity {

Private  EdiText Correo;
Private  EdiText Contrasenia;
Private Button Avanzar;
Private  EdiText Token;
Private TextView Crearcuenta;


@Override
Protected void OnCreate(){
setContentView(R.layout.activitymain);
CrearCuenta=(TexView) findViewById(R.id.txtCrearcuenta);
Correo=(EdiText) findViewById(R.id.edtCorreo);
Contraseña=(EdiText) findViewById(R.id.edtContrasenia);(l
Avanzar=(Button) findViewById(R.id.btnAvanzar);
SharedPreferences preferencias = getSharePreferences(name “credenciales”, Context.MODE_PRIVATE);
If(token !=””){
startActivity(new Intent(Login.this,Menu.class));
Toast.makeText(this,”Bienvenido Crack”, Toast.LENGHT_SHORT).show(); 	
}

CrearCuenta.setOnClickListener(v){
Intent Registro = new Intent(Login.this, Registro.class);
startActivity(Registro);

Login.SetOnclickListener(v){
If (TextUtils.isEmpty(Correo.getText().toString()))
Correo.setError(“Campo Vacio”);
If (TextUtils.isEmpty(Contraseña.getText().toString()))
Contraseña.setError(“Campo Vacio”);
Else{
servicioPeticion service  = Api.getApi(Login.this).create(servicioPeticion.class);
Call<Peticion_Login> peticion logincall = service.login(correro.geText().toString()
Petición_loginCall.eneque(new Callback<Peticion_Login(){

@Override
Public void onResponse(Call<Peticion_Login>call, Response<Peticion_Login> response){
Peticion_Login petición = responsable.body();
If(responsable.body()==null){
Toast.makeText(Login.this, “Creo que tines un error crack”, Toast.LENGHT_SHORT).show();
Return;
}
If(petición.estado == “true”){
ApiToken = petición.token;
guardaPreferencias();
startActivivty(new Intent login.this,Menu.class));
Toast.makeText(Login.this, “Bienvenido crack”, Toast.LENGHT_SHORT).show();
}else{
Toast.makeText(Login.this, petición,estado,Toast.LENGHT_SHORT).show();

	

















PETICION_LOGIN

Public String estado;
Public String correo;
Public String pasword;
Public String token;

Public Peticion_Login(String correo, String passwrod){
This.correo = correo;
This.password = password;
}
Public String getEstado() {return estado;}
Public void getEstado(String estado) {this.estado = estado}
Public String getCorreo() {return correo;}
Public void getCorreo(String correo) {this.correo = correo}
Public String getPassword() {return passwrod;}
Public void getPasword(String password) {this.password = password}
Public String getToken() {return token;}
Public void getToken(String token {this.token = token}


 




CLASE API

public class Api {

    private static final String BASEURL = "https://notificacionupt.andocodeando.net/";
    public static String token = "";

    public Api(){

    }

    private static Retrofit retrofit = null;

    public static Retrofit getApi(Context context){
        if(retrofit == null){
            /*
            SharedPreferences preferencias = context.getSharedPreferences("credenciales", Context.MODE_PRIVATE);
            token = preferencias.getString("TOKEN", "");
            */

            OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    Request newRequest  = chain.request().newBuilder()
                            //.addHeader("Authorization", "Bearer " + token)
                            .build();
                    return chain.proceed(newRequest);
                }
            }).build();

            retrofit = new Retrofit.Builder()
                    .client(client)
                    .baseUrl(BASEURL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            return retrofit;
        }else {
            return retrofit;
        }
    }
}












SERVICIOPETICION.JAVA

Import retrofit2.Call;
Import retrofit2.http.Field;
Import retrofit2.http.FormUr\Encoded;
Import retrofit2.http.POST;

Public interface servicioPeticion {
@FormUr\Encoded
@POST(“api/crearUsuario”)
Call<Registro_Usuario>registrarUsuario(@Field(“username”) String correo, @Field(“Password) String contrasenia);

@FormUr\Encoded
@POST(“api/login)
Call<Peticion _Login> login(@Field(“username”) String correo, @Field(“Password) String contrasenia);
@POST(“api/todosUsuarios”)
Call<Peticion_Usuarios> getUsuarios();

@FormUr\Encoded
@POST(“api/detallesUsuario”)
Call<Detalle_Usuario> detallesUsuario(@Field(“usuarioId”) Int usuarioId);




